package com.atm.modules;

import java.text.NumberFormat;

public class Accounts {

    private String name;
    private float balance;
    private int accountNum;


    public Accounts(){}

    public Accounts(String name, float balance, int accountNum){
        this.name = name;
        this.balance = balance;
        this.accountNum = accountNum;
    }

    //Getter

    public String getName(){
        return this.name;
    }

    public float getBalance(){
        return this.balance;
    }

    public int getAccountNum(){
        return this.accountNum;
    }


    public void setName(String name){
        this.name = name;
    }

    public void setBalance(float balance){
        this.balance = balance;
    }

    public void setAccountNum(int accountNum){
        this.accountNum = accountNum;
    }


    public void withdraw(float amount, int accountNum){
        if (accountNum==this.accountNum){
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            if (amount>50000){
                System.out.println("Maximum withdrawal amount is only P50,000.00. Please try again.");
            }
            else if(amount> this.balance){
                System.out.println("Insufficient Balance. Cannot withdraw!");
            }
            else{
                this.balance -= amount;
                System.out.println("Withdrawn :"+formatter.format(amount));
                this.balance -=500;
                System.out.println("Malacañang Tax:"+formatter.format(500));
                System.out.println("Remaining Balance: "+ formatter.format(this.balance));
            }
        }
        else {
            System.out.println("Invalid account!");
        }
    }

    public void deposit(float amount, int accountNum){

        if (accountNum==this.accountNum){
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            System.out.println("Previous Balance: "+ formatter.format(this.balance));
            this.balance += amount;
            System.out.println("Deposited :"+formatter.format(amount));
            this.balance -=500;
            System.out.println("Malacañang Tax:"+formatter.format(500));
            System.out.println("Remaining Balance: "+ formatter.format(this.balance));
        }
    }

}
