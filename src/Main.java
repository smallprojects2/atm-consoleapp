import com.atm.modules.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;


public class Main {


    public static void main(String[] args) throws Exception {

        Scanner input = new Scanner(System.in);

        //Initialized accounts
        Accounts cauyan = new Accounts("Thomas Cyril Cauyan", 50000, 123456);
        Accounts mocha = new Accounts("Mocha Cauyan", 150000, 246810);

        ArrayList<Accounts> accounts = new ArrayList<>();
        accounts.add(cauyan);
        accounts.add(mocha);



        //get input accountnum
        int acctNum;
        int attempt = 1;
        AtomicReference<Boolean> loggedIn = new AtomicReference<>(false);
        AtomicReference<Accounts> loginAccount = new AtomicReference<>();

        //validate account number if wrong user is allowed 2 more attempts.
        loginLoop: while (attempt<=3){
            System.out.println("Enter Account number: ");
            Boolean acctFormatCorrect = input.hasNextInt();
            if (!acctFormatCorrect){
//                throw new Exception("Invalid account format");
                System.out.println("Invalid account format");
                input.nextLine();
            }
            else {
                acctNum = input.nextInt();
                int finalAcctNum = acctNum;
                accounts.forEach((account) -> {
                    if (finalAcctNum == (account.getAccountNum())) {
                        System.out.println("----------------------------------------");
                        System.out.println("|Account No: " + finalAcctNum + " " + account.getName() + "|");
                        System.out.println("----------------------------------------");
                        loggedIn.set(true);
                        loginAccount.set(account);
                    }
                });

                //breaks the loop if logged in with valid account
                if (loggedIn.get()) {
                    break loginLoop;
                }

                //message changes depending on number of attempts
                if (attempt <= 2) {
                    System.out.println("Wrong account number!" + attempt + " attempt");

                    input.nextLine();
                } else {
                    System.out.println("Wrong account number!");
                    System.out.println("Reached maximum (3) attempts.");
                }
                attempt++;
            }
        }// end of login loop

        //MAIN PROGRAM UPON SUCCESSFUL LOGIN
        if (loggedIn.get()){
            Boolean exit = false;
            System.out.println("****************************");
            System.out.println("WELCOME TO MALACAÑANG BANK!");
            System.out.println("****************************");

            //DO WHILE LOOP
            do {

            System.out.println("What do you want to do?");
            System.out.println("Press [c] to check your balance. [w] to withdraw. [d] to deposit.");
            System.out.println("Type 'exit' to quit.");
            String action = input.next();

            if (action.charAt(0)=='c'){
                checkBalance(loginAccount.get());
                Scanner rinput = new Scanner(System.in);
                while(true){
                    System.out.println("Do you want another transaction? Type 'y'-yes or 'n' no");
                    String response = rinput.next();
                    if(response.charAt(0)=='y'){
                        break;
                    }
                    else if (response.charAt(0)=='n') {

                        System.out.println("Exiting program...");
                        exit=true;
                        break;
                    }
                    else{
                        System.out.println("Invalid command");
                        rinput.nextLine();
                    }
                }

            } else if (action.charAt(0)=='w') {
                //get input amount
                Scanner winput = new Scanner(System.in);
                Boolean correctAccountFormat = false;
                while (!correctAccountFormat){
                    System.out.println("Enter amount to withdraw(maximum 50000): ");
                    correctAccountFormat = winput.hasNextFloat();
                    if(correctAccountFormat){
                        break;
                    }
                    else{
                        System.out.println("Invalid amount format!");
                        winput.nextLine();
                    }

                }

                float withdrawAmount = winput.nextFloat();
                withdraw(withdrawAmount, loginAccount.get());
                Scanner rinput = new Scanner(System.in);
                while(true){
                    System.out.println("Do you want another transaction? Type 'y'-yes or 'n' no");
                    String response = rinput.next();
                    if(response.charAt(0)=='y'){
                        break;
                    }
                    else if (response.charAt(0)=='n') {

                        System.out.println("Exiting program...");
                        exit=true;
                        break;
                    }
                    else{
                        System.out.println("Invalid command");
                        rinput.nextLine();
                    }
                }

            } else if (action.charAt(0)=='d') {

                Scanner dinput = new Scanner(System.in);
                Boolean correctAmountFormat = false;
                float depositAmount;
                while (!correctAmountFormat){
                    System.out.println("Enter amount to deposit: ");
                    correctAmountFormat = dinput.hasNextFloat(); //will return true if user input is float
                    if(correctAmountFormat){

                        break;
                    }
                    else{
                        System.out.println("Invalid amount format!");
                        dinput.nextLine();
                    }
                }
                depositAmount = dinput.nextFloat();
                deposit(depositAmount, loginAccount.get());

                Scanner rinput = new Scanner(System.in);
                while(true){
                    System.out.println("Do you want another transaction? Type 'y'-yes or 'n' no");
                    String response = rinput.next();
                    if(response.charAt(0)=='y'){
                        break;
                    }
                    else if (response.charAt(0)=='n') {

                        System.out.println("Exiting program...");
                        exit=true;
                        break;
                    }
                    else{
                        System.out.println("Invalid command");
                        rinput.nextLine();
                    }
                }
            } else if (action.equals("exit")) {
                System.out.println("Exiting program...");
                exit = true;
            }
            else{
                System.out.println("Invalid command.");
            }
            } while (exit==false);
//        }
        }//end of main program

    }

    //functions
        private static void checkBalance(Accounts account){
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            String moneyString = formatter.format(account.getBalance());
            System.out.println("Your remaining balance is: "+moneyString);
        }

        private static void withdraw(float amount, Accounts account){
            account.withdraw(amount, account.getAccountNum());
        }

        private static void deposit(float amount, Accounts account){
            account.deposit(amount, account.getAccountNum());
        }



}